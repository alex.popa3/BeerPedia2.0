//
//  CachedMethod+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedMethod {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedMethod> {
        return NSFetchRequest<CachedMethod>(entityName: "CachedMethod")
    }

    @NSManaged public var duration: Int16
    @NSManaged public var fermentation: CachedFermentation
    @NSManaged public var mash_temp: NSSet
    @NSManaged public var origin: NSSet
    
    
    var mashTemps : [CachedMashTemp] {
        let mash = mash_temp as? Set<CachedMashTemp> ?? []
        return  mash.sorted {
            $0.temp.unit < $1.temp.unit
        }
    }

}

// MARK: Generated accessors for mash_temp
extension CachedMethod {

    @objc(addMash_tempObject:)
    @NSManaged public func addToMash_temp(_ value: CachedMashTemp)

    @objc(removeMash_tempObject:)
    @NSManaged public func removeFromMash_temp(_ value: CachedMashTemp)

    @objc(addMash_temp:)
    @NSManaged public func addToMash_temp(_ values: NSSet)

    @objc(removeMash_temp:)
    @NSManaged public func removeFromMash_temp(_ values: NSSet)

}

// MARK: Generated accessors for origin
extension CachedMethod {

    @objc(addOriginObject:)
    @NSManaged public func addToOrigin(_ value: CachedBeer)

    @objc(removeOriginObject:)
    @NSManaged public func removeFromOrigin(_ value: CachedBeer)

    @objc(addOrigin:)
    @NSManaged public func addToOrigin(_ values: NSSet)

    @objc(removeOrigin:)
    @NSManaged public func removeFromOrigin(_ values: NSSet)

}

extension CachedMethod : Identifiable {

}
