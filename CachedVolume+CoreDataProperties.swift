//
//  CachedVolume+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedVolume {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedVolume> {
        return NSFetchRequest<CachedVolume>(entityName: "CachedVolume")
    }

    @NSManaged public var unit: String
    @NSManaged public var value: Double
    @NSManaged public var origin: NSSet?

}

// MARK: Generated accessors for origin
extension CachedVolume {

    @objc(addOriginObject:)
    @NSManaged public func addToOrigin(_ value: CachedBeer)

    @objc(removeOriginObject:)
    @NSManaged public func removeFromOrigin(_ value: CachedBeer)

    @objc(addOrigin:)
    @NSManaged public func addToOrigin(_ values: NSSet)

    @objc(removeOrigin:)
    @NSManaged public func removeFromOrigin(_ values: NSSet)

}

extension CachedVolume : Identifiable {

}
