//
//  CachedHops+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedHops {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedHops> {
        return NSFetchRequest<CachedHops>(entityName: "CachedHops")
    }

    @NSManaged public var name: String
    @NSManaged public var amount: CachedAmount
    @NSManaged public var origin: NSSet?

}

// MARK: Generated accessors for origin
extension CachedHops {

    @objc(addOriginObject:)
    @NSManaged public func addToOrigin(_ value: CachedIngredients)

    @objc(removeOriginObject:)
    @NSManaged public func removeFromOrigin(_ value: CachedIngredients)

    @objc(addOrigin:)
    @NSManaged public func addToOrigin(_ values: NSSet)

    @objc(removeOrigin:)
    @NSManaged public func removeFromOrigin(_ values: NSSet)

}

extension CachedHops : Identifiable {

}
