//
//  CachedAmount+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedAmount {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedAmount> {
        return NSFetchRequest<CachedAmount>(entityName: "CachedAmount")
    }

    @NSManaged public var unit: String
    @NSManaged public var value: Double
    @NSManaged public var originHops: NSSet?
    @NSManaged public var originMalt: NSSet?
    
    

}

// MARK: Generated accessors for originHops
extension CachedAmount {

    @objc(addOriginHopsObject:)
    @NSManaged public func addToOriginHops(_ value: CachedHops)

    @objc(removeOriginHopsObject:)
    @NSManaged public func removeFromOriginHops(_ value: CachedHops)

    @objc(addOriginHops:)
    @NSManaged public func addToOriginHops(_ values: NSSet)

    @objc(removeOriginHops:)
    @NSManaged public func removeFromOriginHops(_ values: NSSet)

}

// MARK: Generated accessors for originMalt
extension CachedAmount {

    @objc(addOriginMaltObject:)
    @NSManaged public func addToOriginMalt(_ value: CachedMalt)

    @objc(removeOriginMaltObject:)
    @NSManaged public func removeFromOriginMalt(_ value: CachedMalt)

    @objc(addOriginMalt:)
    @NSManaged public func addToOriginMalt(_ values: NSSet)

    @objc(removeOriginMalt:)
    @NSManaged public func removeFromOriginMalt(_ values: NSSet)

}

extension CachedAmount : Identifiable {

}
