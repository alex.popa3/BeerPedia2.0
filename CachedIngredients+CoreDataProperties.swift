//
//  CachedIngredients+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedIngredients {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedIngredients> {
        return NSFetchRequest<CachedIngredients>(entityName: "CachedIngredients")
    }

    @NSManaged public var hops: NSSet
    @NSManaged public var malt: NSSet
    @NSManaged public var origin: NSSet?
    
    var hopArray: [CachedHops] {
        let array = hops as? Set<CachedHops> ?? []
        return array.sorted {
            $0.name < $1.name
        }
    }
    
    var maltArray: [CachedMalt] {
        let array = malt as? Set<CachedMalt> ?? []
        return array.sorted {
            $0.name < $1.name
        }
    }

}

// MARK: Generated accessors for hops
extension CachedIngredients {

    @objc(addHopsObject:)
    @NSManaged public func addToHops(_ value: CachedHops)

    @objc(removeHopsObject:)
    @NSManaged public func removeFromHops(_ value: CachedHops)

    @objc(addHops:)
    @NSManaged public func addToHops(_ values: NSSet)

    @objc(removeHops:)
    @NSManaged public func removeFromHops(_ values: NSSet)

}

// MARK: Generated accessors for malt
extension CachedIngredients {

    @objc(addMaltObject:)
    @NSManaged public func addToMalt(_ value: CachedMalt)

    @objc(removeMaltObject:)
    @NSManaged public func removeFromMalt(_ value: CachedMalt)

    @objc(addMalt:)
    @NSManaged public func addToMalt(_ values: NSSet)

    @objc(removeMalt:)
    @NSManaged public func removeFromMalt(_ values: NSSet)

}

// MARK: Generated accessors for origin
extension CachedIngredients {

    @objc(addOriginObject:)
    @NSManaged public func addToOrigin(_ value: CachedBeer)

    @objc(removeOriginObject:)
    @NSManaged public func removeFromOrigin(_ value: CachedBeer)

    @objc(addOrigin:)
    @NSManaged public func addToOrigin(_ values: NSSet)

    @objc(removeOrigin:)
    @NSManaged public func removeFromOrigin(_ values: NSSet)

}

extension CachedIngredients : Identifiable {

}
