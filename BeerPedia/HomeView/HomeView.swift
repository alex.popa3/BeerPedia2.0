//
//  ContentView.swift
//  BeerPedia
//
//  Created by Rawfish on 11/04/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = ViewModel()
    @EnvironmentObject var settingsViewModel: SettingsViewModel
    @EnvironmentObject var authManager: AuthManager
    @EnvironmentObject var favoriteViewModel: FavoriteViewModel
    @Environment(\.dismiss) var dismiss

    var body: some View {
        NavigationStack {
            ScrollView(showsIndicators: false) {
                LazyVStack {
                    ForEach(viewModel.state
                        .beers, id: \.id)
                    { beer in
                        NavigationLink {
                            BeerDetailView(beer: beer, fromMainNavigation: true)
                                .environmentObject(viewModel)
                                .environmentObject(favoriteViewModel)
                            
                        } label: {
                            BeerCell(beer: beer)
                                .onAppear {
                                    viewModel.loadMoreContentIfNeeded(beer: beer)
                                }
                        }
                    }
                    .buttonStyle(.plain)
                }
                .offset(y: 100)
                .animation(.none)
                .frame(maxWidth: .infinity)
                .ignoresSafeArea()
            }
            .ignoresSafeArea()
            
            .navigationViewStyle(.columns)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 10) {
                        Button {
                            viewModel.loadRandomBeer()
                        } label: { Image(systemName: "mug")
                                .foregroundColor(.label)
                                .fontWeight(.semibold)
                        }
                        .disabled(viewModel.unloadedData == true ? true : false)
                        .opacity(viewModel.unloadedData ? 0.25 : 1.0)
                        
                        Button {
                            viewModel.showFavoritesBeer = true
                        } label: { Image(systemName: "star")
                                .foregroundColor(.label)
                                .fontWeight(.semibold)
                                .disabled(!favoriteViewModel.data.isEmpty ? true : false)
                        }
                        
                        Button {
                            viewModel.showSettings = true
                            
                        } label: { Image(systemName: "gear")
                                .foregroundColor(.label)
                                .fontWeight(.semibold)
                        }
                    }
                    
                    .padding(.trailing, 5)
                    .padding(2.5)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                    .shadow(radius: 2.5, x: 2.5)
                    .padding(.bottom, 10)
                    .ignoresSafeArea()
                    .offset(x: 12)
                }
            }
            
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    HStack {
                        Text(Bundle.main.appName)
                            .font(.title.bold())
                    }
                    
                    .frame(maxWidth: .infinity)
                    .padding([.trailing, .leading], 10)
                    .background(.regularMaterial)
                    .cornerRadius(40)
                    .padding(.bottom, 10)
                }
            }
            
            .beerTheme()
            .toolbarBackground(AnyShapeStyle(.orange.opacity(1.0)), for: .navigationBar)
        }
       
        //Overlay for showing when the beers are fetched from the DB
        .overlay(viewModel.animationLoading == false ? nil : ApiLoadingView(textOnLoading: "We are fetching the beers..."))
        
        //Sheet for showing settings
        .fullScreenCover(isPresented: $viewModel.showSettings) {
            SettingsView()
                .presentationDetents([.large])
                .environmentObject(settingsViewModel)
                .environmentObject(authManager)
        }
        //Show a full screen page for the random beer fetched from the server, the binding toggleRandomBeer became true when the pipeline receive the completion and turn it to true
        .fullScreenCover(isPresented: $viewModel.toggleRandomBeer) { BeerDetailView(beer: viewModel.state.beer ?? BeerElement.MockedBeer, fromMainNavigation: false)
            .environmentObject(viewModel)
            .environmentObject(favoriteViewModel)
        }
        //Favorites BeerView
        .fullScreenCover(isPresented: $viewModel.showFavoritesBeer) {
            FavoritesView()
                .environmentObject(favoriteViewModel)
                .environmentObject(viewModel)
        }
        
        .alert(isPresented: $viewModel.showAlertConnection) {
            Alert(title: Text("No Internet Connection"), message: Text("Please check your internet connection"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(FavoriteViewModel())
    }
}
