//
//  HomeViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 11/04/23.
//

import Combine
import Foundation
import NotificationCenter
import SwiftUI

class ViewModel: ObservableObject {
    @Published private(set) var state = State()
    @Published var showIngredients = false
    @Published var showValues = false
    @Published var showAlert = false
    @Published var toggleRandomBeer = false
    @Published var showSettings = false
    @Published var showAlertConnection = false
    @Published var unloadedData = false
    @Published var animationLoading = false
    @Published var showFavoritesBeer = false
    @Published var selectedBeer: CachedBeer?
        
    private var subscriptions = Set<AnyCancellable>()
    let feedback = UIImpactFeedbackGenerator(style: .rigid)
    let favorites = FavoriteViewModel.shared.data
    
    //This var is to emulate a reccomandations array
    var reccomandation: [BeerElement] {
        return state.beers.sorted { $0.unwrappedBoilVolume < $1.unwrappedBoilVolume }.suffix(5)
    }

    init() {
    
        
        let apiLoading = Api.networkActivitySubject
            .receive(on: RunLoop.main)
            .sink(receiveValue: { loading in
                if loading {
                    self.animationLoading = true
                } else {
                    self.animationLoading = false
                }
            })
            
            .store(in: &subscriptions)
        
        fetchBeers()
    }
    
    func loadMoreContentIfNeeded(beer: BeerElement) {
        let thresholdIndex = state.beers.index(state.beers.endIndex, offsetBy: -2)
        if state.beers.firstIndex(where: { $0.id == beer.id }) == thresholdIndex {
            fetchBeers()
            
        }
    }
    
    func loadRandomBeer() {
        Api.loadRandomBeer()
            .sink(receiveCompletion: { completion in
                      switch completion {
                      case .failure:
                          print("Errore")
                      case .finished:
                          self.toggleRandomBeer = true
                          print("Random Beer loaded")
                      }
                  },
                  receiveValue: { beer in
                var randomBeer = BeerElement.MockedBeer
                beer.map { beer in
                    randomBeer = BeerElement(id: beer.id, name: beer.name, tagline: beer.tagline, first_brewed: beer.first_brewed, description: beer.description, image_url: beer.unwrappedImage, abv: beer.abv, ibu: beer.ibu, target_fg: beer.target_fg, target_og: beer.target_og, ebc: beer.ebc, srm: beer.srm, ph: beer.ph, attenuation_level: beer.attenuation_level, volume: beer.volume, boil_volume: beer.boil_volume, method: beer.method, ingredients: beer.ingredients, food_pairing: beer.food_pairing, brewers_tips: beer.brewers_tips, contributed_by: beer.contributed_by)
                }
                self.state.beer = randomBeer
                  })
            .store(in: &subscriptions)
    }
    
    func fetchBeers() {
        guard state.canLoadNextPage else { return }
        
        Api.loadBeers(page: state.page)
            .sink(receiveCompletion: onReceive,
                  receiveValue: onReceive)
            .store(in: &subscriptions)
    }
    
    private func onReceive(_ completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            Api.networkActivitySubject.send(false)
        case .failure:
            state.canLoadNextPage = false
            showAlertConnection = true
            unloadedData = true
        }
    }
    
    private func onReceive(_ batch: [BeerElement]) {
        state.beers += batch
        state.page += 1
        state.canLoadNextPage = batch.count == Api.loadedBeerPeerPageSubject.value
        
    }
    
    struct State {
        var beers: [BeerElement] = []
        var page: Int = 1
        var canLoadNextPage = true
        var beer: BeerElement? = nil
        
        
    }
}
