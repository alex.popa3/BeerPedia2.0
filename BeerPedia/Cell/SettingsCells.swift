//
//  SettingsCells.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//

import Foundation
import SwiftUI

struct InfoCell: View {
    var body: some View {
        VStack {
            HStack {
                Text("\(Bundle.main.appName) version:")
                Spacer()
                Text(Bundle.main.appVersion)
                    .fontWeight(.semibold)
            }
        }
        .frame(maxWidth: .infinity)
        .padding()
        .background(.regularMaterial)
        .cornerRadius(30)
        
        HStack {
            Text("Build version:")
            Spacer()
            Text(Bundle.main.build)
                .fontWeight(.semibold)
        }
        .frame(maxWidth: .infinity)
        .padding()
        .background(.regularMaterial)
        .cornerRadius(30)
    }
}
