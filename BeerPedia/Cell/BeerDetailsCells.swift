//
//  BeerDetailsCells.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//

import Foundation
import SwiftUI

//Beer's values cell

struct BeerValueCell: View {
    let value: String
    let title: String
    
    var body: some View {
        HStack {
            Text("\(title):")
            Spacer()
            Text("\(value)")
        }
        .padding(15)
        .background(.regularMaterial)
        .cornerRadius(30)
        .padding(15)
    }
}
