//
//  BeerCell.swift
//  BeerPedia
//
//  Created by Rawfish on 12/04/23.
//

import SwiftUI

struct BeerCell: View {
    let beer: BeerElement
    
    var body: some View {
        HStack(alignment: .top) {
            Spacer()
            VStack {
                AsyncImage(
                    url: URL(string: beer.unwrappedImage),
                    content: { image in
                        image.resizable()
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .shadow(radius: 10)
                    },
                    placeholder: {
                        if beer.unwrappedImage != "no image" {
                            ProgressView()
                        } else {
                            VStack {
                                Image(systemName: "photo.on.rectangle")
                                    .resizable()
                                    .frame(maxWidth: 50, maxHeight: 45)
                                    .padding(.bottom, 10)
                                Text("No image to load")
                                    .font(.system(size: 11))
                            }
                        }
                    }
                )
               
            }.frame(height: 150)
                .padding(10)
            
            Spacer()
            
            HStack(alignment: .center) {
                VStack(alignment: .leading, spacing: 10.0) {
                    Text(beer.name)
                        .lineLimit(1)
                        .font(.callout)
                        .fontWeight(.semibold)
                    
                    Text(beer.description)
                        .font(.caption)
                        .lineLimit(2)
                }
            }
            .offset(x: 10, y: 20.0)
            Spacer()
        }
        .onAppear { print("L'URL è :\(beer.unwrappedImage)") }
        .frame(maxHeight: 170)
        .padding(.all, 15)
        .background(.regularMaterial)
        .cornerRadius(40)
        .padding(.all)
        .shadow(color: .black.opacity(0.50), radius: 1.5)
    }
}
