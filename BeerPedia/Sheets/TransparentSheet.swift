//
//  CustomSheets + View.swift
//  BeerPedia
//
//  Created by Rawfish on 02/05/23.
//

import SwiftUI

extension View {
    func transparentSheet<Content: View>(_ style: AnyShapeStyle, show: Binding<Bool>, @ViewBuilder content: @escaping () -> Content) -> some View {
        self
            .sheet(isPresented: show) {
                content()
                    .background(RemoveBackgroundColor())
                    .frame(maxWidth: .infinity)
                    .background(
                        Rectangle()
                            .fill(style)
                            .cornerRadius(30)
                            .shadow(radius: 5)
                            .ignoresSafeArea(.container, edges: .all))
                    .padding(10)
                  
            }
    }
}

private struct RemoveBackgroundColor: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        return UIView()
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        DispatchQueue.main.async {
            uiView.superview?.superview?.backgroundColor = .clear
        }
    }
}
