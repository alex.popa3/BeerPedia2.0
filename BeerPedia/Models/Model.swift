//
//  Model.swift
//  BeerPedia
//
//  Created by Rawfish on 11/04/23.
//

import Foundation

// MARK: - BeerElement

struct BeerElement: Codable, Equatable, Hashable, Identifiable {
    let id: Int
    let name: String
    let tagline: String
    let first_brewed: String
    let description: String
    let image_url: String?
    let abv: Double?
    let ibu: Double?
    let target_fg: Double?
    let target_og: Double?
    let ebc: Double?
    let srm: Double?
    let ph: Double?
    let attenuation_level: Double?
    let volume: Boil_Volume?
    let boil_volume: Boil_Volume?
    let method: Method?
    let ingredients: Ingredients?
    let food_pairing: [String]?
    let brewers_tips: String?
    let contributed_by: String?
    var favorite: Bool {
        let favorite = FavoriteViewModel.shared.data
        if favorite.contains(where: { $0.id == self.id}) == true {
            return true
        } else {
            return false
        }
    }

    static let MockedBeer = BeerElement(id: 1, name: "Punk IPA 2007 - 2010", tagline: "Post Modern Classic. Spiky. Tropical. Hoppy.", first_brewed: "04/2007", description: "Our flagship beer that kick started the craft beer revolution. This is James and Martin's original take on an American IPA, subverted with punchy New Zealand hops. Layered with new world hops to create an all-out riot of grapefruit, pineapple and lychee before a spiky, mouth-puckering bitter finish.", image_url: "https://images.punkapi.com/v2/keg.png", abv: 5.0, ibu: 6.0, target_fg: 6.0, target_og: 6.0, ebc: 5.60, srm: 5.1, ph: 1.0, attenuation_level: 5.1, volume: Boil_Volume(value: 4.5, unit: "Kg"), boil_volume: Boil_Volume(value: 1.3, unit: "volume"), method: Method(mash_temp: [Mash_temp(temp: Temp(value: 1.3, unit: "temp"), duration: 14)], fermentation: Fermentation(temp: Temp(value: 12, unit: "ex"))), ingredients: Ingredients(malt: [Malt(name: "malt1", amount: Amount(value: 23.31, unit: "kg"))], hops: [Hop(name: "hOPW", amount: Amount(value: 12.3, unit: "kG"), add: "", attribute: "")], yeast: "Wyeast 1056 - American Ale™"), food_pairing: [
        "Spicy carne asada with a pico de gallo sauce",
        "Shredded chicken tacos with a mango chilli lime salsa",
        "Cheesecake with a passion fruit swirl sauce"
    ], brewers_tips: "While it may surprise you, this version of Punk IPA isn't dry hopped but still packs a punch! To make the best of the aroma hops make sure they are fully submerged and add them just before knock out for an intense hop hit.", contributed_by: "Sam Mason <samjbmason>")
}

// MARK: - BoilVolume

struct Boil_Volume: Codable, Equatable, Hashable, Comparable {
    let value: Double?
    let unit: String?

    static func < (lhs: Boil_Volume, rhs: Boil_Volume) -> Bool {
        return lhs.formattedValue < rhs.formattedValue
    }
}

struct Amount: Codable, Equatable, Hashable {
    let value: Double?
    let unit: String?
}

// MARK: - Ingredients

struct Ingredients: Codable, Equatable, Hashable {
    let malt: [Malt]
    let hops: [Hop]
    let yeast: String?
}

// MARK: - Hop

struct Hop: Codable, Equatable, Hashable {
    let name: String?
    let amount: Amount?
    let add: String?
    let attribute: String?
}

// MARK: - Malt

struct Malt: Codable, Equatable, Hashable {
    let name: String?
    let amount: Amount?
}

// MARK: - Method

struct Method: Codable, Equatable, Hashable {
    let mash_temp: [Mash_temp]
    let fermentation: Fermentation?
}

// MARK: - Fermentation

struct Fermentation: Codable, Equatable, Hashable {
    let temp: Temp
}

// MARK: - MashTemp

struct Mash_temp: Codable, Equatable, Hashable {
    let temp: Temp
    let duration: Int?
}

struct Temp: Codable, Equatable, Hashable {
    let value: Double?
    let unit: String?
}


