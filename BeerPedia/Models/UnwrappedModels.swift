//
//  UnwrappedModels.swift
//  BeerPedia
//
//  Created by Rawfish on 13/04/23.
//

import Foundation

extension BeerElement {
    var unwrappedImage: String {
        self.image_url ?? "no image"
    }
    
      var unwrappedAbv: Double {
        self.abv ?? 0.0
    }
    
    var formattedAbv: String {
        String(format: "%1.0f", self.unwrappedAbv)
    }
    
      var unwrappedIbu: Double {
        self.ibu ?? 0.0
    }
    
    var formattedIbu: String {
        String(format: "%0.1f", self.unwrappedIbu)
    }
    
      var unwrappedTargetFG: Double {
        self.target_fg ?? 0.0
    }
    
    var formattedTargetFg: String {
        String(format: "%0.1f", self.unwrappedTargetFG)
    }
   
      var unwrappedTargetOg: Double {
        self.target_og ?? 0.0
    }
    
    var formattedTargetOg: String {
        String(format: "%0.1f", self.unwrappedTargetOg)
    }
    
      var unwrappedEbc: Double {
        self.ebc ?? 0.0
    }
    
    var formattedEbc: String {
        String(format: "%0.1f", self.unwrappedEbc)
    }
    
      var unwrappedSrm: Double {
        self.srm ?? 0.0
    }
    
    var formattedSrm: String {
        String(format: "%0.1f", self.unwrappedSrm)
    }

      var unwrappedPh: Double {
        self.ph ?? 0.0
    }
    
    var formattedPh: String {
        String(format: "%0.1f", self.unwrappedPh)
    }

      var unwrappedAttenuationLevel: Double {
        self.attenuation_level ?? 0.0
    }
    
    var formattedAttenuationLevel: String {
        String(format: "%0.1f", self.unwrappedAttenuationLevel)
    }

    var unwrappedVolume: Boil_Volume {
        self.volume ?? Boil_Volume(value: 0.0, unit: "")
    }

    var unwrappedBoilVolume: Boil_Volume {
        self.boil_volume ?? Boil_Volume(value: 0.0, unit: "")
    }

    var unwrappedMethod: Method {
        self.method ?? Method(mash_temp: [Mash_temp(temp: Temp(value: 0.0, unit: ""), duration: 0)], fermentation: Fermentation(temp: Temp(value: 0.0, unit: "")))
    }
    
    var unwrappedIngredients: Ingredients {
        self.ingredients ?? Ingredients(malt: [Malt(name: "", amount: Amount(value: 0.0, unit: ""))], hops: [Hop(name: "", amount: Amount(value: 0.0, unit: ""), add: "", attribute: "")], yeast: "")
    }
    
    var unwrappedFoodPairing: [String] {
        self.food_pairing ?? ["",""]
    }
    
    var unwrappedBrewerTips: String {
        self.brewers_tips ?? ""
    }
    
    var unwrappedcontributedBy: String {
        self.contributed_by ?? ""
    }
}

extension Boil_Volume {
     var unwrappedValue: Double {
        self.value ?? 0.0
    }
    
    var formattedValue: String {
        String(format: "%0.1f", self.unwrappedValue)
    }
    
    var unwrappedUnit: String {
        self.unit ?? ""
    }
}

extension Amount {
     var unwrappedValue: Double {
        self.value ?? 0.0
    }
    
    var formattedValue: String {
        String(format: "%0.1.f", self.unwrappedValue)
    }
    
    var unwrappedUnit: String {
        self.unit ?? ""
    }
}

extension Malt {
    var unwrappedName: String {
        self.name ?? ""
    }

    var unwrappedAmount: Amount {
        self.amount ?? Amount(value: 0.0, unit: "")
    }
}

extension Hop {
     var unwrappedName: String {
        self.name ?? ""
    }

     var unwrappedAmount: Amount {
        self.amount ?? Amount(value: 0.0, unit: "")
    }
    
    var unwrappedAdd: String {
        self.add ?? ""
    }
    
    var unwrappedAttribute: String {
        self.attribute ?? ""
    }
}

extension Method {
    var unwrappedFermentation: Fermentation {
        self.fermentation ?? Fermentation(temp: Temp(value: 0.0, unit: ""))
    }
}

extension Temp {
     var unwrapppedValue: Double {
        self.value ?? 0.0 as Double
    }
    
    var formattedValue: String {
        String(format: "%0.1f", self.unwrapppedValue)
    }
   
    var unwrappedUnit: String {
        self.unit ?? ""
    }
}

extension Mash_temp {
    var unwrappedDuration: Int {
        self.duration ?? 0
    }
}
