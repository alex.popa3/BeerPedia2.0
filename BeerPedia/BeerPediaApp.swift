//
//  BeerPediaApp.swift
//  BeerPedia
//
//  Created by Rawfish on 11/04/23.
//

import CoreData
import SwiftUI
@main
struct BeerPediaApp: App {
    @StateObject var authManager = AuthManager()
    @StateObject var favoritesViewModel = FavoriteViewModel.shared
   
    var body: some Scene {
        WindowGroup {
            BeerPediaAppRoute()
                .environmentObject(authManager)
                .environmentObject(favoritesViewModel)
        }
    }
}

struct BeerPediaAppRoute: View {
    @EnvironmentObject var authViewModel: AuthManager
     
    @StateObject var viewModel = ViewModel()
    @StateObject var settingViewModel = SettingsViewModel()
    @EnvironmentObject var favoritesViewModel: FavoriteViewModel
    
    var body: some View {
        if authViewModel.isLogged == false {
            LoginView()
                .animation(.easeInOut(duration: 1.0))
                .environmentObject(authViewModel)
                .environmentObject(viewModel)
                .environmentObject(settingViewModel)
                .transition(.opacity.combined(with: .slide))
            
        } else {
            ContentView()
                .environmentObject(viewModel)
                .environmentObject(settingViewModel)
                .environmentObject(favoritesViewModel)
                
                .transition(.opacity)
        }
    }
}
