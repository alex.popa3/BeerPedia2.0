//
//  Colors.swift
//  BeerPedia
//
//  Created by Rawfish on 12/04/23.
//

import Foundation
import SwiftUI

extension Color {
    
    static let adaptiveOrange = Color("AdaptiveOrange")
    static let label = Color("Label")
}
