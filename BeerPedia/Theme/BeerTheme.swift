//
//  BeerTheme.swift
//  BeerPedia
//
//  Created by Rawfish on 12/04/23.
//

import Foundation
import SwiftUI

struct BeerTheme: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .background(.linearGradient(stops: [
                                                Gradient.Stop(color: .yellow, location: 0.40),
                                                Gradient.Stop(color: .adaptiveOrange, location: 0.60)], startPoint: .bottomTrailing, endPoint: .topLeading))
            .animation(.none)
    }
}



 

extension View {
    func beerTheme() -> some View {
        modifier(BeerTheme())
    }
    
   
    
     
    
}
