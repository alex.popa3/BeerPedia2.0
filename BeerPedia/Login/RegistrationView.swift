//
//  RegistrationView.swift
//  BeerPedia
//
//  Created by Rawfish on 26/04/23.
//

import SwiftUI

struct RegistrationView: View {
    @StateObject var viewModel = RegistrationViewModel()
    @EnvironmentObject var authManager : AuthManager
    @Environment(\.dismiss) var dismiss
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack() {
            Text("Registration")
                    .font(.largeTitle.bold())
                                
                TextField("First name", text: $viewModel.nameTextField)
                    .logintextFieldAppearance()
                TextField("Last name", text: $viewModel.surnameTextFiedl)
                    .logintextFieldAppearance()
                HStack {
                    Text("Select your age")
                        .padding(.leading,10)
                    Spacer()
                    Picker("Your Age", selection: $viewModel.userAge) {
                        ForEach(16...90,id: \.self) {
                            Text("\($0)")
                        }
                    }.background(.regularMaterial)
                        .tint(.label)
                    .cornerRadius(30)
                   
                    
                    
                }
                
                .padding(-7)
                .logintextFieldAppearance()
                .shadow(color: viewModel.colorValidation, radius: 3)
                
                if viewModel.userAge < 18 {
                    
                    HStack {
                        Text("You need ho be 18 years old")
                            .font(.system(size: 11))
                        Image(systemName: "18.circle")
                        
                    }
                    .padding(10)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                    .frame(maxWidth: .infinity)
                    .foregroundColor(.red)
                    .fontWeight(.semibold)
                    .shadow(radius: 5)
                    .padding([.top,.bottom],5)
                    .animation(.easeInOut(duration: 1.5),value: viewModel.errorPassword)
                    .transition(.opacity)
                    
                }
                
                TextField("Email", text: $viewModel.emailTextField)
                    .logintextFieldAppearance()
                
                
                VStack(alignment:.leading) {
                    
                    SecureField("Password",text: $viewModel.passwordTextField)
                        .logintextFieldAppearance()
                        
                   
                    SecureField("Confirm password",text: $viewModel.confirmedPassword)
                        .logintextFieldAppearance()
                        .shadow(color: viewModel.passwordValidate == true ? .green : .label.opacity(0.0) , radius: 3)
                    
                    if viewModel.errorPassword == true {
                         
                        ErrorInformationAlert(errorString: viewModel.errorBadPassword)
                      
                    }
                    
                   
                        if viewModel.passwordValidate == true {
                            withAnimation {
                                HStack {
                                    Text("Valid password!")
                                        .font(.system(size: 12))
                                        .animation(.easeIn(duration: 1.0))
                                    Image(systemName: "v.circle")
                                    
                                }
                               
                                .padding(10)
                                .background(.regularMaterial)
                                .cornerRadius(30)
                                .frame(maxWidth: .infinity)
                                .foregroundColor(.green)
                                .fontWeight(.semibold)
                                .shadow(radius: 5)
                                .padding([.top,.bottom],5)
                                .animation(.easeInOut(duration: 1.5))
                                .transition(.opacity)
                            }
                        }
                    
                    
                    if viewModel.errorPassword && viewModel.errorBadPassword == "Invalid password" {
                        VStack(alignment:.leading){
                            Text("The password must contain:")
                            Text("a minimum of 1 lower case letter [a-z] and")
                            Text("a minimum of 1 upper case letter [A-Z]")
                            Text("a minimum of 1 numeric character [0-9]")
                            Text("a minimum of 1 special character")
                            Text("must be at least 8 characters")
                        }
                        .foregroundColor(.gray)
                        .font(.system(size: 13.5))
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(.regularMaterial)
                        .cornerRadius(30)
                        .padding([.leading,.trailing],15)
                        .animation(.easeIn(duration: 1.5))
                    }
                }
                .padding(.top,20)
                
                VStack(spacing: 10) {
                    HStack {
                        Link(destination: URL(string:"https://www.termsfeed.com/public/uploads/2021/12/sample-terms-conditions-agreement.pdf")!) {
                            
                            Text("Terms & Conditions")
                                .font(.system(size: 15))
                            Image(systemName: "link.circle")
                                .foregroundColor(.accentColor)
                                
                                .lineLimit(1)
                        }
                           
                        Spacer()
                        
                        Toggle("", isOn: $viewModel.aceptedTerms)
                            .frame(maxWidth: 50)
                    }
                    HStack {
                        Link( destination: URL(string: "https://www.termsfeed.com/public/uploads/2021/12/sample-privacy-policy-template.pdf")!) {
                            Text("Policy privacy")
                                .font(.system(size: 15))
                            Image(systemName: "link.circle")
                                .foregroundColor(.accentColor)
                                
                                .lineLimit(1)
                        }
                            
                       
                        Spacer()
                        Toggle("", isOn: $viewModel.aceptedPolicyPrivacy)
                            .frame(maxWidth: 50)
                    }
                }
               
                .padding(20)
                .background(.regularMaterial)
                .cornerRadius(30)
                .padding([.leading, .trailing], 10)

            }
            
            .padding(.top,20)
            
            if viewModel.allFieldValidates {
                Button("Complete Registration") {
                    dismiss()
                    authManager.isLogged = true
                    authManager.statusLogin()
                    
                    
                }
                
                .disabled(viewModel.allFieldValidates == true ? false : true )
                .logintextFieldAppearance()
                .offset(y:50)
                .transition(.opacity.animation(.easeOut(duration: 1.25)))
                .shadow(radius: 5)
                .opacity(viewModel.allFieldValidates == true ? 1.0 : 0.0)
               
                
            }
           
           
            Spacer()
        }.beerTheme()
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
            .environmentObject(AuthManager())
          
    }
}

struct ErrorInformationAlert: View {
    let errorString : String
    var body: some View {
        HStack {
            Text(errorString)
                .font(.system(size: 11))
                .animation(.easeIn(duration: 1.0))
            Image(systemName: "x.circle")
            
        }
        .padding(10)
        .background(.regularMaterial)
        .cornerRadius(30)
        .frame(maxWidth: .infinity)
        .foregroundColor(.red)
        .fontWeight(.semibold)
        .shadow(radius: 5)
        .padding([.top,.bottom],5)
        .animation(.easeInOut(duration: 1.5))
        .transition(.opacity)
    }
}
