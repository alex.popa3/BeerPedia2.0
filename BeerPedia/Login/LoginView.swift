//
//  LoginView.swift
//  BeerPedia
//
//  Created by Rawfish on 19/04/23.
//

import SwiftUI

struct LoginView: View {
    @StateObject var viewModel = LoginViewModel()
    @EnvironmentObject var homeViewModel: ViewModel
    @EnvironmentObject var authManager: AuthManager
    @EnvironmentObject var settingsViewModel: SettingsViewModel
    
    var body: some View {
        GeometryReader { fullSize in
            ZStack {
                GeometryReader { geo in
                    HStack {
                        Image("55")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: geo.size.height / 2.55)
                            .offset(x: fullSize.size.width - geo.size.width * 1.0, y: fullSize.size.height - geo.size.height / 1.50)
                            .shadow(color: .label, radius: 200)
                            .animation(.easeIn)
                            .transition(.opacity.combined(with: .slide))
                        
                        Image("120")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: geo.size.height / 2.25)
                            .offset(x: fullSize.size.width - geo.size.width * 2.20, y: fullSize.size.height - geo.size.height / 1.10)
                            .shadow(color: .label, radius: 200)
                            .animation(.easeIn)
                            .transition(.opacity.combined(with: .slide))
                    }
                }
                
                VStack(spacing: 10) {
                    Text(Bundle.main.appName)
                        .font(.largeTitle)
                        .foregroundColor(.label)
                        .bold()
                        .padding(.top, 10)
                        .onTapGesture(count: 5) {
                            viewModel.feedback.impactOccurred()
                            if viewModel.debuggedLogin == false {
                                viewModel.debuggedLogin = true
                            } else
                            { viewModel.debuggedLogin = false }
                        }
                    VStack {
                        TextField("Email", text: $viewModel.email)
                            .logintextFieldAppearance()
                      
                        TextField("Password", text: $viewModel.password)
                            .logintextFieldAppearance()
                    }
                    
                    if viewModel.debuggedLogin == true {
                        Button("Enter bypass") {
                            authManager.isLogged = true
                        }
                        .background(.regularMaterial)
                    }
                    
                    Spacer()
                    
                    Button("Registration") {
                        viewModel.showRegistrationSheet = true
                    }
                    .logintextFieldAppearance()
                    
                    Button("Login") {
                        authManager.isLogged = true
                        authManager.statusLogin()
                    }
                    .animation(.none)
                    .frame(maxWidth: viewModel.isEnable == false ? 50 : 150)
                    .logintextFieldAppearance()
                    .animation(.easeIn(duration: 1.25))
                    .disabled(viewModel.isEnable == true ? false : true)
                    .fullScreenCover(isPresented: $authManager.isLogged) {
                        ContentView()
                            .environmentObject(homeViewModel)
                    }
                    Spacer()
                    
                    HStack {
                       
                        Text("\(Bundle.main.appName) verions: \(Bundle.main.appVersion)")
                             
                        
                            
                        
                        Spacer()
                    }
                    .padding([.leading],20)
                }
                .sheet(isPresented: $viewModel.showRegistrationSheet) {
                    RegistrationView()
                        .environmentObject(authManager)
                }
            }
           
            .beerTheme()
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
            .environmentObject(AuthManager())
    }
}
