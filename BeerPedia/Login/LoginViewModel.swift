//
//  LoginViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 19/04/23.
//

import Combine
import CoreHaptics
import Foundation
import SwiftUI

class LoginViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var isEnable: Bool = false
    @Published var confirmed18yearsOld = false
    @Published var showRegistrationSheet = false
    @Published var debuggedLogin = false
    let feedback = UIImpactFeedbackGenerator(style: .medium)
    
    private var subscriptions = Set<AnyCancellable>()

    init() {
        enableLogin
            .receive(on: RunLoop.main)
            .assign(to: \.isEnable, on: self)
            .store(in: &subscriptions)
    }
    
    private var enableLogin: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest($email, $password)
            .throttle(for: 1.0, scheduler: DispatchQueue.main, latest: true)
            .map { email, password in
                email.contains("@") && !password.isEmpty
            }
            .eraseToAnyPublisher()
    }
}

class RegistrationViewModel: ObservableObject {
    @Published var nameTextField = ""
    @Published var surnameTextFiedl = ""
    
    @Published var userAge = 18
    @Published var confirmedMajorAge: Bool = false

    @Published var emailTextField = ""
    @Published var userInformationsValidate = false
    
    @Published var passwordTextField = ""
    @Published var confirmedPassword = ""
    
    @Published var passwordValidate = false

    @Published var aceptedTerms: Bool = false
    @Published var aceptedPolicyPrivacy = false
    
    @Published var aceptedAllTerms = false
    
    @Published var allFieldValidates = false
    
    @Published var errorPassword = true
    
    var errorBadPassword: String = "Invalid password"
    
    @Published var documentSheet: Bool = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    let passwordValidation = NSPredicate(format: "SELF MATCHES %@ ", "^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).{8,}$")

    @Published var colorValidation: Color = .clear
    
    init() {
        userInformations
            .receive(on: DispatchQueue.main)
            .assign(to: \.userInformationsValidate, on: self)
            .store(in: &subscriptions)
        
        majorAgeConfirmed
            .receive(on: DispatchQueue.main)
            .assign(to: \.confirmedMajorAge, on: self)
            .store(in: &subscriptions)
        
        matchPassword
            .receive(on: DispatchQueue.main)
            .assign(to: \.passwordValidate, on: self)
            .store(in: &subscriptions)
        
        allTermsAccepted
            .receive(on: DispatchQueue.main)
            .assign(to: \.aceptedAllTerms, on: self)
            .store(in: &subscriptions)
        
        validateAllFields
            .receive(on: DispatchQueue.main)
            .assign(to: \.allFieldValidates, on: self)
            .store(in: &subscriptions)
    }
    
    private var userInformations: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest3($nameTextField, $surnameTextFiedl, $emailTextField)
            .map { name, surname, email in
                !name.isEmpty && !surname.isEmpty && !email.isEmpty && email.contains("@")
            }
            .eraseToAnyPublisher()
    }
    
    var matchPassword: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest($passwordTextField, $confirmedPassword)
            .throttle(for: 5.0, scheduler: RunLoop.main, latest: true)
            .map { password, confirmedPassword in
                if password.isEmpty && confirmedPassword.isEmpty {
                    self.errorPassword = false
                }
                if !password.isEmpty && confirmedPassword.isEmpty {
                    self.errorPassword = false
                }
                if password.count < 3 || confirmedPassword.count < 3 {
                    self.errorPassword = false
                }
                else if confirmedPassword != password { self.errorPassword = true
                    self.errorBadPassword = "Passwords do not match, please try again"
                }
                
                else { self.errorPassword = false }
                
                return !password.isEmpty && !confirmedPassword.isEmpty && self.passwordValidation.evaluate(with: password) == true && password == confirmedPassword && !password.contains(self.nameTextField) && confirmedPassword == password
            }
            .eraseToAnyPublisher()
    }
    
    var majorAgeConfirmed: AnyPublisher<Bool, Never> {
        $userAge
            .map { age in
                if age >= 18 {
                    self.colorValidation = .green
                    return true
                }
                else { self.colorValidation = .red
                    return false
                }
            }
            .eraseToAnyPublisher()
    }
    
    var allTermsAccepted: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest($aceptedTerms, $aceptedPolicyPrivacy)
            .map { terms, policy in
                terms == true && policy == true
            }
            .eraseToAnyPublisher()
    }
    
    var validateAllFields: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest4($userInformationsValidate, $confirmedMajorAge, $passwordValidate, $aceptedAllTerms)
            .map { userInfo, confirmedAge, password, terms in
                userInfo == true && confirmedAge == true && password == true && terms == true
            }
            .eraseToAnyPublisher()
    }
}
