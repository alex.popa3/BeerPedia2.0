//
//  LoginModifiers.swift
//  BeerPedia
//
//  Created by Rawfish on 19/04/23.
//

import SwiftUI

struct TextFieldsAppearence: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .padding(15)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading,.trailing],10)
    }
}


extension View {
    func logintextFieldAppearance() -> some View {
        modifier(TextFieldsAppearence())
    }
}
