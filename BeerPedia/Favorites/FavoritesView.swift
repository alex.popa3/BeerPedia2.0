//
//  FavoritesView.swift
//  BeerPedia
//
//  Created by Rawfish on 27/04/23.
//

import CoreData
import SwiftUI

struct FavoritesView: View {
    @EnvironmentObject var viewModel: FavoriteViewModel
    @EnvironmentObject var homeViewModel: ViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            if viewModel.data.isEmpty {
                VStack {
                    VStack {
                     EmptyFavoritesList()
                    }
                    .padding()
                    .background(.regularMaterial)
                    .cornerRadius(30)
                
                }.frame(maxWidth: .infinity, maxHeight: .infinity)
                    .animation(.default)
                    .transition(.opacity)
               
            } else {
                NavigationView {
                    ScrollView {
                        LazyVStack {
                            HStack(spacing: 1) {
                                Button(action: { dismiss() }, label: {
                                    Image(systemName: "arrow.left")
                                        .resizable()
                                        .frame(width: 35, height: 30)
                                        .foregroundStyle(.regularMaterial)
                                })
                              
                                Spacer()
                                Button {
                                    viewModel.showAllDeleteAlert = true
                                } label: {
                                    HStack {
                                        Text("Delete all")
                                        Image(systemName: "trash")
                                            .resizable()
                                            .frame(width: 25, height: 25)
                                    }
                                    .foregroundColor(.red)
                                    .padding()
                                    .background(.regularMaterial.opacity(0.15))
                                    .cornerRadius(30)
                                }
                            }
                            .font(.system(.body, design: .default, weight: .medium))
                            .frame(maxWidth: .infinity)
                            .padding([.leading, .trailing], 10)
                            ForEach(viewModel.data, id: \.name) { cachedBeer in
                                 
                                NavigationLink { BeerDetailView(beer: BeerElement(cachedBeer: cachedBeer) ?? BeerElement.MockedBeer, fromMainNavigation: false)
                                    .environmentObject(homeViewModel)
                                    .environmentObject(viewModel)
                                } label: {
                                    BeerCell(beer: BeerElement(cachedBeer: cachedBeer) ?? BeerElement.MockedBeer)
                                }
                            }
                        }
                        .padding(.top, 20)
                    }
                    .transparentSheet(AnyShapeStyle(.regularMaterial), show: $viewModel.showAllDeleteAlert) {
                        ErrorAlert(title: "Delete all beer", description: "Are you sure to delete all beers?", action: viewModel.deleteAllData)
                            .environmentObject(viewModel)
                            .presentationDetents([.fraction(0.15)])
                    }
                    .transparentSheet(AnyShapeStyle(.regularMaterial), show: $viewModel.showDeleteAlert) {
                        ErrorAlert(title: "Delete beer", description: "Are you sure to remove the beer from favorites?", action: viewModel.deleteBeer)
                            .environmentObject(viewModel)
                            .presentationDetents([.fraction(0.15)])
                    }
                    .beerTheme()
                    .buttonStyle(.plain)
                }
                .animation(.default)
                .transition(.opacity)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .beerTheme()
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView()
            .environmentObject(ViewModel())
            .environmentObject(FavoriteViewModel())
    }
}
