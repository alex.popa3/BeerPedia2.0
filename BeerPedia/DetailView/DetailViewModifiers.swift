//
//  DetailViewModifiers.swift
//  BeerPedia
//
//  Created by Rawfish on 13/04/23.
//

import Foundation
import SwiftUI

struct DetailPageContainers: ViewModifier {
    
    let padding: CGFloat
    
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity)
            .padding(padding)
            .background(.regularMaterial)
            .cornerRadius(30)
            .shadow(color: .black.opacity(0.50), radius: 1.5)
            .padding(padding)
    }
}


struct DetailPageButtons: ViewModifier {
    
    func body(content: Content) -> some View {
        content
        .frame(width: 50, height: 50)
        .background(.ultraThickMaterial)
        .cornerRadius(30)
        .foregroundColor(.adaptiveOrange)
        .shadow(color: .black.opacity(0.50), radius: 1.5)
    }
}


extension View {
    func container(padding: CGFloat) -> some View {
        modifier(DetailPageContainers(padding: padding))
    }
    func detailPageButtons() -> some View {
        modifier(DetailPageButtons())
    }
}
