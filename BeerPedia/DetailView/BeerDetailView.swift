//
//  BeerDetailView.swift
//  BeerPedia
//
//  Created by Rawfish on 12/04/23.
//

import SwiftUI

struct BeerDetailView: View {
    var beer: BeerElement
    var selectedFavoriteBeer: CachedBeer? {
        let favoriteBeer = favoriteViewModel.data.first(where: {$0.id == beer.id})
        return favoriteBeer
    }
    let fromMainNavigation: Bool
    @EnvironmentObject var viewModel: ViewModel
    @EnvironmentObject var favoriteViewModel: FavoriteViewModel

    @Environment(\.dismiss) var dismiss
    var body: some View {
        GeometryReader { fullsize in
            ZStack(alignment: .top) {
                GeometryReader { geo in
                    AsyncImage(
                        url: URL(string: beer.unwrappedImage),
                        content: { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geo.size.height / 2.25)
                                .offset(x: fullsize.size.width - geo.size.width * 1.30, y: fullsize.size.height - geo.size.height / 1.10)
                                .shadow(color: .black.opacity(0.25), radius: 200)
                                .animation(.easeInOut(duration: 2.0))
                        },
                        placeholder: {
                            if beer.unwrappedImage != "no image" {
                                ProgressView()
                            }
                        }
                    )
                }

                ScrollView(showsIndicators: false) {
                    VStack {
                        HStack(spacing: fullsize.frame(in: .local).midX / 11) {
                            Button { dismiss()

                            } label: {
                                Image(systemName: "chevron.backward")
                                    .detailPageButtons()
                            }

                            Text(beer.name)
                                .frame(width: 200)
                                .padding(15)
                                .background(.regularMaterial)
                                .cornerRadius(30)
                                .shadow(color: .black.opacity(0.50), radius: 1.5)
                                .fontWeight(.semibold)

                            Button {
                                if beer.favorite == nil || beer.favorite == false {
                                    favoriteViewModel.addData(beer: beer)
                                    viewModel.feedback.impactOccurred()
                                   
                                } else {
                                    favoriteViewModel.selectedBeer = selectedFavoriteBeer
                                    print(selectedFavoriteBeer)
                                    favoriteViewModel.showDeleteAlert = true
                                    viewModel.feedback.impactOccurred()
                                }

                            } label: {
                                if beer.favorite == nil || beer.favorite == false {
                                    Image(systemName: "star")
                                        .detailPageButtons()
                                } else if beer.favorite == true {
                                    Image(systemName: "star.fill")
                                        .detailPageButtons()
                                }
                            }
                        }
                        .frame(maxWidth: .infinity)
                    }
                    .padding(.top, 15)

                    VStack {
                        Text(beer.tagline)

                            .padding(1)
                            .font(.system(size: 13, weight: .semibold))
                            .lineLimit(1, reservesSpace: true)
                        Text(beer.first_brewed)
                            .font(.system(size: 13, weight: .regular))
                    }

                    .container(padding: 10)
                    .padding(.top, 15)

                    VStack(alignment: .leading) {
                        Text("Description")
                            .font(.title2.bold())
                            .padding([.top, .leading], 10)
                            .padding(.bottom, 5)
                            .shadow(color: .black.opacity(0.50), radius: 1.0)

                        Text(beer.description)
                            .padding([.leading, .trailing, .bottom], 10)
                    }

                    .container(padding: 10)
                    .padding(5)

                    HStack {
                        Button { viewModel.showIngredients = true } label: {
                            HStack {
                                Image(systemName: "mug")
                                    .foregroundColor(.adaptiveOrange)
                                Text("Ingredients")
                            }
                        }.frame(maxWidth: .infinity)
                            .padding(10)
                            .background(.thickMaterial)
                            .cornerRadius(30)

                        Button { viewModel.showValues = true } label: {
                            HStack {
                                Image(systemName: "info.circle")
                                    .foregroundColor(.adaptiveOrange)
                                Text("Values")
                            }
                        }.frame(maxWidth: .infinity)
                            .padding(10)
                            .background(.thickMaterial)
                            .cornerRadius(30)
                    }
                    .buttonStyle(.plain)

                    .fontWeight(.medium)
                    .container(padding: 10)
                    if fromMainNavigation == true {
                        VStack(alignment: .leading) {
                            Text("Reccomended beers")
                                .padding(10)
                                .font(.title2)
                                .fontWeight(.semibold)
                                .shadow(color: .black.opacity(0.50), radius: 1.5)
                                .background(.regularMaterial)
                                .cornerRadius(30)
                                .offset(x: 20, y: 7)

                            ScrollView(.horizontal, showsIndicators: false) {
                                HStack(spacing: 10) {
                                    ForEach(viewModel.reccomandation) { beer in
                                        NavigationLink {
                                            BeerDetailView(beer: beer, fromMainNavigation: true)
                                                .environmentObject(viewModel)
                                                .environmentObject(favoriteViewModel)
                                        } label: {
                                            VStack {
                                                AsyncImage(
                                                    url: URL(string: beer.image_url ?? ""),
                                                    content: { image in
                                                        image
                                                            .resizable()
                                                            .aspectRatio(contentMode: .fit)
                                                            .frame(width: 40, height: 70)
                                                            .shadow(color: .label.opacity(0.25), radius: 20)
                                                    },
                                                    placeholder: {
                                                        ProgressView()
                                                    }
                                                )
                                                Text(beer.name)
                                                    .font(.callout)
                                                    .fontWeight(.semibold)
                                                    .foregroundColor(.adaptiveOrange)
                                                    .offset(y: 20)
                                            }
                                            .frame(width: 160, height: 160)
                                            .padding()
                                            .background(.thickMaterial)
                                            .cornerRadius(30)
                                            .padding(10)
                                        }
                                    }
                                }
                                .shadow(radius: 1, x: 0, y: 0)
                            }

                            .shadow(color: .black.opacity(0.50), radius: 1.5)
                            .background(.regularMaterial)
                            .cornerRadius(30)
                            .padding([.leading, .top, .bottom, .trailing], 20)
                        }
                    }
                }
                .overlay { viewModel.animationLoading == false ? nil :
                    ApiLoadingView(textOnLoading: "Stiamo prendendo la tua birra")
                }
            }
            .transparentSheet(AnyShapeStyle(.regularMaterial), show: $favoriteViewModel.showDeleteAlert) {
                ErrorAlert(title: "Delete beer", description: "Are you sure to remove the beer from favorites?", action: favoriteViewModel.deleteBeer)
                    .environmentObject(viewModel)
                    .presentationDetents([.fraction(0.15)])
            }
            .sheet(isPresented: $viewModel.showIngredients) {
                BeerDetailSheets(beer: beer)
                    .environmentObject(viewModel)
                    .presentationDetents([.fraction(0.75)])
            }
            .sheet(isPresented: $viewModel.showValues) {
                BeerValuesSheet(beer: beer)
                    .environmentObject(viewModel)
                    .presentationDetents([.fraction(0.60)])
            }
            .onAppear { print(beer.unwrappedImage)
                print("Il valore del bool favorite è : \(beer.favorite)")
            }
            .navigationBarBackButtonHidden(true)
            .beerTheme()
        }.animation(.easeInOut(duration: 2.0))
    }
}

struct BeerDetailView_Previews: PreviewProvider {
    static var previews: some View {
        BeerDetailView(beer: BeerElement.MockedBeer, fromMainNavigation: true)
            .environmentObject(ViewModel())
    }
}
