//
//  AuthViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 21/04/23.
//

import Foundation
import Combine

class AuthManager: ObservableObject {
    
    @Published var isLogged = false
    @Published var logout = false
    @Published var login = false
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        
        isLogged = UserDefaults.standard.bool(forKey: "statusLogin")
        
        let authService = AuthService.logoutProcess
            .receive(on: RunLoop.main)
            .sink(receiveValue: { logout in
                if logout == true {
                    AuthService.logoutProcess.send(false)
                    self.isLogged = false
                    UserDefaults.standard.set(self.isLogged, forKey: "statusLogin")
                }
                
                
            })
            .store(in: &subscriptions)
    }
    
    func statusLogin() {
        
        UserDefaults.standard.set(isLogged, forKey: "statusLogin")
    }
}
