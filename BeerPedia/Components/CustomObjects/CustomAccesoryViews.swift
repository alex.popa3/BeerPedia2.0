//
//  CustomAccesoryViewa.swift
//  BeerPedia
//
//  Created by Rawfish on 20/04/23.
//

import SwiftUI

struct ApiLoadingView: View {
    let textOnLoading: String
    var body: some View {
        VStack(alignment: .center) {
            Text(textOnLoading)
                .padding(10)
                .font(.system(size: 10, weight: .semibold))
                .foregroundColor(.adaptiveOrange)
                .background(.regularMaterial)
                .cornerRadius(30)
                .shadow(color:.adaptiveOrange,radius: 2)
               
            ProgressView()
                .shadow(color:.adaptiveOrange,radius: 1)
                
        }
        .padding(15)
        .background(.regularMaterial.opacity(0.50))
        .cornerRadius(10)
        .shadow(radius: 5)
        .scaleEffect(1.5)
        .offset(y: 50)
    }
}

struct CustomAccesoryViewa_Previews: PreviewProvider {
    static var previews: some View {
        ApiLoadingView(textOnLoading: "Stiamo caricando le birre")
    }
}
