//
//  SettingsViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 20/04/23.
//

import SwiftUI

struct SettingsView: View {
    @EnvironmentObject var viewModel: SettingsViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(alignment: .leading) {
                HStack(alignment: .center) {
                    Button(action: { dismiss() }, label: {
                        Image(systemName: "arrow.left")
                            .resizable()
                            .frame(width: 35, height: 30)
                            .foregroundStyle(.regularMaterial)
                    })
                    Text("Settings")
                        .font(.largeTitle)
                        .fontWeight(.medium)
                        .padding(.bottom, 1)
                        .offset(x: 5)
                        .foregroundStyle(.regularMaterial)
                }
                .offset(x: 25)
                
                SettingsCell(settingsDescription: "Beer loaded", pickerAvaible: true, buttonAvaible: false, interactive: false, viewModelValue: $viewModel.loadedBeers)
                
                SettingsCell(settingsDescription: "Terms & Conditions", pickerAvaible: true, buttonAvaible: true, interactive: true, viewModelValue: $viewModel.termsAndConditions)
        
                SettingsCell(settingsDescription: "BeerPedia Info", pickerAvaible: false, buttonAvaible: false, interactive: true, viewModelValue: $viewModel.infoDialog)
                
                Spacer()
                SettingsCell(settingsDescription: "Logout", pickerAvaible: false, buttonAvaible: false, interactive: true, viewModelValue: $viewModel.logout)
            }
            .transparentSheet(AnyShapeStyle(Color.clear), show: $viewModel.infoDialog, content: { InfoCell()
                    .presentationDetents([.fraction(0.15)])
            })
           
            .transparentSheet(AnyShapeStyle(Color.clear), show: $viewModel.termsAndConditions, content: {
                LegalStuffDocumentsView()
                    .presentationDetents([.fraction(0.15)])
            })
            .presentationDetents([.fraction(0.99)])
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .beerTheme()
    }
}

struct SettingsViewModel_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
            .environmentObject(SettingsViewModel())
    }
}

struct SettingsCell<T: Hashable>: View {
    let settingsDescription: String
    let pickerAvaible: Bool
    let buttonAvaible: Bool
    let interactive: Bool
    @Binding var viewModelValue: T
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        if interactive == true {
            Button { viewModelValue = true as! T
               
            } label: {
                HStack {
                    if settingsDescription == "Logout" {
                        Spacer()
                    }
                    Text(settingsDescription)
                        .fontWeight(settingsDescription == "Logout" ? .black : .semibold)
                    Spacer()
                }
                
                .settingsCellModifier(settingTitle: settingsDescription)
            }
           
            .buttonStyle(.plain)
        } else if interactive == false {
            HStack {
                Text(settingsDescription)
                    .fontWeight(.medium)
                
                if buttonAvaible == true {
                    Button {} label: {
                        Image(systemName: "info.circle")
                            .foregroundColor(.adaptiveOrange)
                    }
                }
                
                if pickerAvaible == true {
                    Spacer()
                    Picker("", selection: $viewModelValue) {
                        ForEach(10 ... 80, id: \.self) {
                            Text("\($0)")
                        }
                    }
                    .background(.orange)
                    .cornerRadius(30)
                    .accentColor(.label)
                }
            }
            
            .settingsCellModifier(settingTitle: settingsDescription)
        }
    }
}

struct LegalStuffDocumentsView: View {
    var body: some View {
        VStack {
            HStack {
                Link(destination: URL(string: "https://www.termsfeed.com/public/uploads/2021/12/sample-terms-conditions-agreement.pdf")!) {
                    Text("Terms & Conditions")
                        .font(.system(size: 15))
                    
                    Spacer()
                    Image(systemName: "link.circle")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(.accentColor)
                        
                        .lineLimit(1)
                }
            }
            .frame(maxWidth: .infinity)
            .padding()
            .background(.regularMaterial)
            .cornerRadius(30)
             
            HStack {
                Link(destination: URL(string: "https://www.termsfeed.com/public/uploads/2021/12/sample-privacy-policy-template.pdf")!) {
                    Text("Policy privacy")
                        .font(.system(size: 15))
                    Spacer()
                    Image(systemName: "link.circle")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(.accentColor)
                        
                        .lineLimit(1)
                }
            }
            .frame(maxWidth: .infinity)
            .padding()
            .background(.regularMaterial)
            .cornerRadius(30)
        }
    }
}
