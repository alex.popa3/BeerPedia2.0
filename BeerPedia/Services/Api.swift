//
//  Api.swift
//  BeerPedia
//
//  Created by Rawfish on 11/04/23.
//

import Combine
import Foundation

enum Api {
  
    static let loadedBeerPeerPageSubject = CurrentValueSubject<Int,Never>(10)
    static let networkActivitySubject = PassthroughSubject<Bool, Never>()
    
  
    static func loadRandomBeer() -> AnyPublisher<[BeerElement], Error> {
        print("Static func chiamata")
        let url = URL(string: "https://api.punkapi.com/v2/beers/random")!
    
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .handleEvents(
                receiveSubscription: { _ in networkActivitySubject.send(true) },
                receiveCompletion: { _ in networkActivitySubject.send(false)},
                receiveCancel: { networkActivitySubject.send(false)
                })
            .tryMap { try JSONDecoder().decode([BeerElement].self, from: $0.data) }
            
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    static func loadBeers(page: Int) -> AnyPublisher<[BeerElement], Error> {
        
        let url = URL(string: "https://api.punkapi.com/v2/beers?page=\(page)&per_page=\(loadedBeerPeerPageSubject.value)") ?? URL(string: "https://api.punkapi.com/v2/beers?page=1&per_page=80")!
       
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .delay(for: DispatchQueue.SchedulerTimeType.Stride(integerLiteral: Int.random(in: 1 ..< 5)), scheduler: DispatchQueue.main)
            .retry(2)
            .handleEvents(
                receiveSubscription: { _ in networkActivitySubject.send(true) },
                receiveCompletion: { _ in networkActivitySubject.send(false)},
                receiveCancel: { networkActivitySubject.send(false)
                })
            .tryMap { try JSONDecoder().decode([BeerElement].self, from: $0.data) }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

